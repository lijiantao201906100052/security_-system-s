/*----------------------------------------------------------------------------
 * Copyright (c) <2018>, <Huawei Technologies Co., Ltd>
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 * conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 * of conditions and the following disclaimer in the documentation and/or other materials
 * provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 * to endorse or promote products derived from this software without specific prior written
 * permission.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
 * Notice of Export Control Law
 * ===============================================
 * Huawei LiteOS may be subject to applicable export control laws and regulations, which might
 * include those applicable to Huawei LiteOS of U.S. and the country in which you are located.
 * Import, export and usage of Huawei LiteOS in any manner by you shall be in compliance with such
 * applicable export control laws and regulations.
 *---------------------------------------------------------------------------*/
/**
 *  DATE                AUTHOR      INSTRUCTION
 *  2020-02-05 17:00  zhangqianfu  The first version
 *
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#include "ohos_init.h"
#include "ohos_types.h"
#include "ohos_errno.h"
#include "cmsis_os2.h"

#include "iot_gpio.h"
#include "iot_adc.h"
#include "iot_pwm.h"

#include "sensor.h"
#include "motor.h"
#include "oc_mqtt.h"
#include "wifi_connect.h"
#include "lwip/sockets.h"

#define MSGQUEUE_OBJECTS 16 // 消息队列对象的数量

extern int Window_flag = 0;//窗口标记位 1--开窗 0--关窗
extern int Rain_flag = 0;//雨滴标记位  1--下雨 0--无雨
extern int Curtain_flag = 0;//窗帘标记位 1--开窗帘 0--关窗帘
extern int Fan_flag = 0;//风扇标记位 1--开启 0--关闭
extern int GasBuzzer_flag = 0;//气体报警 1--开启 0--关闭
extern int SmokeBuzzer_flag = 0;//烟雾报警 1--开启 0--关闭

typedef struct
{ // object data type
    char *Buf;
    uint8_t Idx;
} MSGQUEUE_OBJ_t;

MSGQUEUE_OBJ_t msg;
osMessageQueueId_t mid_MsgQueue; // 消息队列ID

//华为云密钥信息
#define CLIENT_ID "62f264ab3a8848355987e25f_20190610005202_0_0_2022080914"
#define USERNAME "62f264ab3a8848355987e25f_20190610005202"
#define PASSWORD "5572c128ccaf3e3b9f85e6c0451a0dbfcb2c3848796e52a2566d9d6cea4ca377"

typedef enum
{
    en_msg_cmd = 0,
    en_msg_report,
} en_msg_type_t;

typedef struct
{
    char *request_id;
    char *payload;
} cmd_t;

typedef struct
{
    int light;
    int smoke;
    int gas;
    int rain
} report_t;

typedef struct
{
    en_msg_type_t msg_type;
    union
    {
        cmd_t cmd;
        report_t report;
    } msg;
} app_msg_t;

typedef struct
{
    int Curtain_flag;
    int SmokeBuzzer_flag;
    int Fan_flag;
    int GasBuzzer_flag;
    int Window_flag;
} app_cb_t;
static app_cb_t g_app_cb;

//属性名称与华为云平台一一对应
static void deal_report_msg(report_t *report)
{
    oc_mqtt_profile_service_t service;
    oc_mqtt_profile_kv_t light;
    oc_mqtt_profile_kv_t smoke;
    oc_mqtt_profile_kv_t gas;
    oc_mqtt_profile_kv_t rain;
    oc_mqtt_profile_kv_t Curtain_flag;
    oc_mqtt_profile_kv_t SmokeBuzzer_flag;
    oc_mqtt_profile_kv_t Fan_flag;
    oc_mqtt_profile_kv_t GasBuzzer_flag;
    oc_mqtt_profile_kv_t Window_flag;

    service.event_time = NULL;
    service.service_id = "Security";
    service.service_property = &light;
    service.nxt = NULL;

    light.key = "light";
    light.value = &report->light;
    light.type = EN_OC_MQTT_PROFILE_VALUE_INT;
    light.nxt = &smoke;

    smoke.key = "smoke";
    smoke.value = &report->smoke;
    smoke.type = EN_OC_MQTT_PROFILE_VALUE_INT;
    smoke.nxt = &gas;

    gas.key = "gas";
    gas.value = &report->gas;
    gas.type = EN_OC_MQTT_PROFILE_VALUE_INT;
    gas.nxt = &rain;

    rain.key = "rain";
    rain.value = &report->rain;
    rain.type = EN_OC_MQTT_PROFILE_VALUE_INT;
    rain.nxt = &Curtain_flag;

    Curtain_flag.key = "Curtain_flag";
    Curtain_flag.value = g_app_cb.Curtain_flag ? "ON" : "OFF";
    Curtain_flag.type = EN_OC_MQTT_PROFILE_VALUE_STRING;
    Curtain_flag.nxt = &SmokeBuzzer_flag;

    SmokeBuzzer_flag.key = "SmokeBuzzer_flag";
    SmokeBuzzer_flag.value = g_app_cb.SmokeBuzzer_flag ? "ON" : "OFF";
    SmokeBuzzer_flag.type = EN_OC_MQTT_PROFILE_VALUE_STRING;
    SmokeBuzzer_flag.nxt = &Fan_flag;

    Fan_flag.key = "Fan_flag";
    Fan_flag.value = g_app_cb.Fan_flag ? "ON" : "OFF";
    Fan_flag.type = EN_OC_MQTT_PROFILE_VALUE_STRING;
    Fan_flag.nxt = &GasBuzzer_flag;

    GasBuzzer_flag.key = "GasBuzzer_flag";
    GasBuzzer_flag.value = g_app_cb.GasBuzzer_flag ? "ON" : "OFF";
    GasBuzzer_flag.type = EN_OC_MQTT_PROFILE_VALUE_STRING;
    GasBuzzer_flag.nxt = &Window_flag;

    Window_flag.key = "Window_flag";
    Window_flag.value = g_app_cb.Window_flag ? "ON" : "OFF";
    Window_flag.type = EN_OC_MQTT_PROFILE_VALUE_STRING;
    Window_flag.nxt = NULL;

    oc_mqtt_profile_propertyreport(USERNAME, &service);
    return;
}

void oc_cmd_rsp_cb(uint8_t *recv_data, size_t recv_size, uint8_t **resp_data, size_t *resp_size)
{
    app_msg_t *app_msg;

    int ret = 0;
    app_msg = malloc(sizeof(app_msg_t));
    app_msg->msg_type = en_msg_cmd;
    app_msg->msg.cmd.payload = (char *)recv_data;

    printf("recv data is %.*s\n", recv_size, recv_data);
    ret = osMessageQueuePut(mid_MsgQueue, &app_msg, 0U, 0U);
    if (ret != 0)
    {
        free(recv_data);
    }
    *resp_data = NULL;
    *resp_size = 0;
}

//华为IoT平台下发命令控制
#include <cJSON.h>
static void deal_cmd_msg(cmd_t *cmd)
{
    cJSON *obj_root;
    cJSON *obj_cmdname;
    cJSON *obj_paras;
    cJSON *obj_para;

    int cmdret = 1;
    oc_mqtt_profile_cmdresp_t cmdresp;
    obj_root = cJSON_Parse(cmd->payload);
    if (NULL == obj_root)
    {
        goto EXIT_JSONPARSE;
    }
    obj_cmdname = cJSON_GetObjectItem(obj_root, "command_name");
    if (NULL == obj_cmdname)
    {
        goto EXIT_CMDOBJ;
    }
    //命令控制---窗帘
    if (0 == strcmp(cJSON_GetStringValue(obj_cmdname), "Security_Control_Curtain"))
    {
        obj_paras = cJSON_GetObjectItem(obj_root, "Paras");
        if (NULL == obj_paras)
        {
            goto EXIT_OBJPARAS;
        }
        obj_para = cJSON_GetObjectItem(obj_paras, "Curtain");
        if (NULL == obj_para)
        {
            goto EXIT_OBJPARA;
        }
        //操作窗帘
        if (0 == strcmp(cJSON_GetStringValue(obj_para), "ON"))
        {
            CurtainStatusSet(ON);//开窗
            g_app_cb.Curtain_flag = 1;
            Curtain_flag = 1;
        }
        else
        {
            CurtainStatusSet(OFF);//关窗
            g_app_cb.Curtain_flag = 0;
            Curtain_flag = 0;
        }
        cmdret = 0;
    }
    else if (0 == strcmp(cJSON_GetStringValue(obj_cmdname), "Security_Control_Window"))
    {
        obj_paras = cJSON_GetObjectItem(obj_root, "Paras");
        if (NULL == obj_paras)
        {
            goto EXIT_OBJPARAS;
        }
        obj_para = cJSON_GetObjectItem(obj_paras, "Window");
        if (NULL == obj_para)
        {
            goto EXIT_OBJPARA;
        }
        ///< operate the Motor here
        if (0 == strcmp(cJSON_GetStringValue(obj_para), "ON"))
        {
            MotorStatusSet(ON);
            g_app_cb.Window_flag = 1;
            Window_flag = 1;
        }
        else
        {
            MotorStatusSet(OFF);
            g_app_cb.Window_flag = 0;
            Window_flag = 0;
        }
        cmdret = 0;
    }

EXIT_OBJPARA:
EXIT_OBJPARAS:
EXIT_CMDOBJ:
    cJSON_Delete(obj_root);
EXIT_JSONPARSE:
    ///< do the response
    cmdresp.paras = NULL;
    cmdresp.request_id = cmd->request_id;
    cmdresp.ret_code = cmdret;
    cmdresp.ret_name = NULL;
    (void)oc_mqtt_profile_cmdresp(NULL, &cmdresp);
    return;
}


static int task_main_entry(void)
{
    app_msg_t *app_msg;

    uint32_t ret = WifiConnect("Mai", "00000000");//wifi账号密码

    device_info_init(CLIENT_ID, USERNAME, PASSWORD);
    oc_mqtt_init();
    oc_set_cmd_rsp_cb(oc_cmd_rsp_cb);

    while (1)
    {
        app_msg = NULL;
        (void)osMessageQueueGet(mid_MsgQueue, (void **)&app_msg, NULL, 0U);
        if (NULL != app_msg)
        {
            switch (app_msg->msg_type)
            {
            case en_msg_cmd:
                deal_cmd_msg(&app_msg->msg.cmd);
                break;
            case en_msg_report:
                deal_report_msg(&app_msg->msg.report);
                break;
            default:
                break;
            }
            free(app_msg);
        }
    }
    return 0;
}

static int task_sensor_entry(void)
{
    app_msg_t *app_msg;
    int light;
    int smoke;
    int gas;
    int rain;

    Light_Init();//光照传感器初始化
    Smoke_Init();//烟雾传感器初始化
    Gas_Init();//气体传感器初始化
    Rain_Init();//雨滴传感器初始化

    BuzzerInit();//蜂鸣器初始化
    MotorInit();//窗户（直流电机）初始化
    FanInit();//风扇（继电器）初始化
    CurtainInit();//窗帘（拓展板电机）初始化

    while (1)
    {
        app_msg = malloc(sizeof(app_msg_t));

        printf("-------------光照传感器------------\r\n");
        light = GetLight();//光照值
        printf("光照强度:%d \r\n", light);

        if(light >= 80 && Curtain_flag == 0){
            printf("光照很强，窗帘关着!\r\n");
            Curtain_flag = 0;
        }
        if(light >= 80 && Curtain_flag == 1){
            printf("光照很强，窗帘开着，即将关闭窗帘!\r\n");
            CurtainStatusSet(OFF);//关窗帘
            Curtain_flag = 0;
            g_app_cb.Curtain_flag = 0;
        }
        if(light <= 30 && Curtain_flag == 1){
            printf("光照很弱，窗帘开着!\r\n");
            Curtain_flag = 1;
        }
        if(light <= 30 && Curtain_flag == 0){
            printf("光照很弱，窗帘关着,即将关闭窗帘!\r\n");
            CurtainStatusSet(ON);//开窗帘
            Curtain_flag = 1;
            g_app_cb.Curtain_flag = 1;
        }
        // if(30<light<80 && Curtain_flag == 0){
        //     printf("光照正常，窗帘关着!\r\n");
        //     Curtain_flag = 0;
        // }
        // if(30 < light <80 && Curtain_flag == 1){
        //     printf("光照正常，窗帘开着!\r\n");
        //     Curtain_flag = 1;
        // }
        printf("窗帘:%d \r\n", Curtain_flag);
        // printf("-----------------------------------\r\n\n");

        printf("-------------烟雾传感器------------\r\n");
        smoke = GetSmoke();//烟雾值
        printf("烟雾浓度:%d \r\n", smoke);

        if(smoke >= 50){
            printf("烟雾数据异常，开启警报!\r\n");
            SmokeBuzzer(ON);//烟雾警报开启
            SmokeBuzzer_flag = 1;
            g_app_cb.SmokeBuzzer_flag = 1;
        }
        else{
            printf("烟雾数据正常!\r\n");
            SmokeBuzzer(OFF);//烟雾警报关闭
            SmokeBuzzer_flag = 0;
            g_app_cb.SmokeBuzzer_flag = 0;
        }
        printf("烟雾警报:%d \r\n", SmokeBuzzer_flag);
        // printf("-----------------------------------\r\n\n");

        printf("-------------气体传感器------------\r\n");
        gas = GetGas();//气体值
        printf("气体浓度:%d \r\n", gas);

        if(gas >= 50){
            printf("气体数据异常，开启警报并且开启抽风机!\r\n");
            GasBuzzer(ON);//气体警报开启
            FanStatusSet(ON);//风扇开启
            Fan_flag = 1;GasBuzzer_flag = 1;
            g_app_cb.Fan_flag = 1;
            g_app_cb.GasBuzzer_flag = 1;
        }
        if(gas < 45){
            printf("气体数据正常!\r\n");
            GasBuzzer(OFF);//气体警报关闭
            FanStatusSet(OFF);//风扇关闭
            Fan_flag = 0;GasBuzzer_flag = 0;
            g_app_cb.Fan_flag = 0;
            g_app_cb.GasBuzzer_flag = 0;
        }
        printf("气体警报:%d \r\n", GasBuzzer_flag);
        printf("风扇:%d \r\n", Fan_flag);
        // printf("-----------------------------------\r\n\n");

        printf("-------------雨滴传感器------------\r\n");
        //雨滴传感器部分
        rain = GetRain();//雨水值
        printf("雨水强度:%d \r\n", rain);
        
        if(rain >= 50){Rain_flag = 1;}//雨滴板值大于等于50--下雨标记位--1--下雨
        if(rain < 50){Rain_flag = 0;}//雨滴板值小于50--无雨标记位--0--无雨

        //无雨标记
        if(Rain_flag == 0 && Window_flag == 0){
            printf("无雨、窗户关着!\r\n");
            Rain_flag = 0;
            Window_flag = 0;
        }
        if(Rain_flag == 0 && Window_flag == 1){
            printf("无雨、窗户开着!\r\n");
            Rain_flag = 0;
            Window_flag = 1; 
        }
        //下雨标记
        if(Rain_flag == 1 && Window_flag == 0){
            printf("下雨、窗户关着!\r\n");
            Rain_flag = 1;
            Window_flag = 0;  
        }
        if(Rain_flag == 1 && Window_flag == 1){
            printf("下雨、窗户开着、即将关窗!\r\n");
            // g_app_cb.motor = 0;
            MotorStatusSet(OFF);//关窗
            g_app_cb.Window_flag = 0;
            Rain_flag = 1;
            Window_flag = 0;
        }
        printf("窗户:%d \r\n", Window_flag);
        printf("下雨状态:%d \r\n", Rain_flag);
        // printf("-----------------------------------\r\n\n");

        if (NULL != app_msg)
        {
            app_msg->msg_type = en_msg_report;
            app_msg->msg.report.light = light;
            app_msg->msg.report.smoke = smoke;
            app_msg->msg.report.gas = gas;
            app_msg->msg.report.rain = rain;

            g_app_cb.Curtain_flag = Curtain_flag;
            g_app_cb.Window_flag = Window_flag;

            if (0 != osMessageQueuePut(mid_MsgQueue, &app_msg, 0U, 0U))
            {
                free(app_msg);
            }
        }

        usleep(5000000);//延时五秒 测一次值
    }
    return 0;
}

static void SecuritySystem(void)
{
    mid_MsgQueue = osMessageQueueNew(MSGQUEUE_OBJECTS, 10, NULL);
    if (mid_MsgQueue == NULL)
    {
        printf("Falied to create Message Queue!\n");
    }

    osThreadAttr_t attr;

    attr.name = "task_sensor_entry";
    attr.attr_bits = 0U;
    attr.cb_mem = NULL;
    attr.cb_size = 0U;
    attr.stack_mem = NULL;
    attr.stack_size = 10240;
    attr.priority = 24;
    if (osThreadNew((osThreadFunc_t)task_main_entry, NULL, &attr) == NULL)
    {
        printf("Falied to create task_main_entry!\n");
    }

    attr.stack_size = 2048;
    attr.priority = 25;
    attr.name = "task_sensor_entry";

    if (osThreadNew((osThreadFunc_t)task_sensor_entry, NULL, &attr) == NULL)
    {
        printf("Falied to create task_sensor_entry!\n");
    }
}

APP_FEATURE_INIT(SecuritySystem);


