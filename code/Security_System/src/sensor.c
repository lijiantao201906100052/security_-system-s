/*----------------------------------------------------------------------------
 * Copyright (c) <2018>, <Huawei Technologies Co., Ltd>
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 * conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 * of conditions and the following disclaimer in the documentation and/or other materials
 * provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 * to endorse or promote products derived from this software without specific prior written
 * permission.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
 * Notice of Export Control Law
 * ===============================================
 * Huawei LiteOS may be subject to applicable export control laws and regulations, which might
 * include those applicable to Huawei LiteOS of U.S. and the country in which you are located.
 * Import, export and usage of Huawei LiteOS in any manner by you shall be in compliance with such
 * applicable export control laws and regulations.
 *---------------------------------------------------------------------------*/
/**
 *  DATE                AUTHOR      INSTRUCTION
 *  2020-02-05 17:00  zhangqianfu  The first version
 *
 */

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <math.h>

#include "cmsis_os2.h"
#include "ohos_init.h"
#include "sensor.h"
#include "iot_errno.h"
#include "iot_gpio.h"
#include "iot_adc.h"

typedef unsigned char   uint8;
typedef unsigned short  uint16;

//光照传感器初始化
void Light_Init(void)
{
    //光照传感器---模拟信号---检测光亮大小---ADC1初始化---GPIO4
    IoTGpioInit(IOT_GPIO_IO_GPIO_4);
    IoTIoSetFunc(IOT_GPIO_IO_GPIO_4, IOT_IO_FUNC_GPIO_4_GPIO);
    IoTGpioSetDir(IOT_GPIO_IO_GPIO_4, IOT_GPIO_DIR_IN);
}

//光照传感器采集数据
uint8 GetLight()
{
    uint8 Light_temp = 0;//百分比的整数值 

    float Light_vol = 0.0;//adc采样电压  
    unsigned short val;
    unsigned int ret;

    //获取ADC1采集值---光照传感器
    ret = IoTAdcRead(IOT_ADC_CHANNEL_1, &val, IOT_ADC_EQU_MODEL_8, IOT_ADC_CUR_BAIS_DEFAULT, 256);

    //将ADC1采集值转换成电压值
    Light_vol = IOTAdcConvertToVoltage(val);//以便数据分析与观察
    printf("光照传感器电压值voltage:%f \r\n", Light_vol);
    Light_vol = 3.45-Light_vol;
    Light_temp = (float)((float)Light_vol) / 3.45 * 1000;

    if(Light_temp <= 0){Light_temp = 0;}
    if(Light_temp >= 100){Light_temp = 100;}

    return Light_temp;
}


//烟雾传感器初始化
void Smoke_Init(void)
{
    //烟雾传感器---模拟信号---检测光亮大小---ADC3初始化---GPIO11
    IoTGpioInit(IOT_GPIO_IO_GPIO_11);
    IoTIoSetFunc(IOT_GPIO_IO_GPIO_11, IOT_IO_FUNC_GPIO_11_GPIO);
    IoTGpioSetDir(IOT_GPIO_IO_GPIO_11, IOT_GPIO_DIR_IN);
}

//烟雾传感器采集数据
uint8 GetSmoke()
{
    uint8 Smoke_temp = 0;//百分比的整数值 

    float Smoke_vol = 0.0;//adc采样电压  
    unsigned short val;
    unsigned int ret;

    //获取ADC5采集值---烟雾传感器
    ret = IoTAdcRead(IOT_ADC_CHANNEL_5, &val, IOT_ADC_EQU_MODEL_8, IOT_ADC_CUR_BAIS_DEFAULT, 256);

    //将ADC5采集值转换成电压值
    Smoke_vol = IOTAdcConvertToVoltage(val);//以便数据分析与观察
    printf("烟雾传感器电压值voltage:%f \r\n", Smoke_vol);
    Smoke_temp = (float)((float)Smoke_vol)*25;

    if(Smoke_temp <= 15){Smoke_temp = 0;}
    if(Smoke_temp >= 100){Smoke_temp = 100;}

    return Smoke_temp;
}


//气体传感器初始化
void Gas_Init(void)
{
    //气体传感器---模拟信号---检测气体异常情况---ADC0初始化---GPIO12
    IoTGpioInit(IOT_GPIO_IO_GPIO_12);
    IoTIoSetFunc(IOT_GPIO_IO_GPIO_12, IOT_IO_FUNC_GPIO_12_GPIO);
    IoTGpioSetDir(IOT_GPIO_IO_GPIO_12, IOT_GPIO_DIR_IN);
}

//气体传感器采集数据
uint8 GetGas()
{
    uint8 Gas_temp = 0;//百分比的整数值 

    float Gas_vol = 0.0;//adc采样电压  
    unsigned short val;
    unsigned int ret;

    //获取ADC0采集值---气体传感器
    ret = IoTAdcRead(IOT_ADC_CHANNEL_0, &val, IOT_ADC_EQU_MODEL_8, IOT_ADC_CUR_BAIS_DEFAULT, 256);

    //将ADC0采集值转换成电压值
    Gas_vol = IOTAdcConvertToVoltage(val);//以便数据分析与观察
    printf("气体传感器电压值voltage:%f \r\n", Gas_vol);
    Gas_temp = (float)((float)Gas_vol)*33;

    if(Gas_temp <= 30){Gas_temp = 0;}
    if(Gas_temp >= 100){Gas_temp = 100;}

    return Gas_temp;
}

//雨滴传感器初始化
void Rain_Init(void)
{
    //雨滴传感器---模拟信号---检测是否下雨---ADC4初始化---GPIO9
    IoTGpioInit(IOT_GPIO_IO_GPIO_9);
    IoTIoSetFunc(IOT_GPIO_IO_GPIO_9, IOT_IO_FUNC_GPIO_9_GPIO);
    IoTGpioSetDir(IOT_GPIO_IO_GPIO_9, IOT_GPIO_DIR_IN);
}

//雨滴传感器采集数据
uint8 GetRain()
{
    uint8 Rain_temp = 0;//百分比的整数值 

    float Rain_vol = 0.0;//adc采样电压  
    unsigned short val;
    unsigned int ret;

    //获取ADC4采集值---雨滴传感器
    ret = IoTAdcRead(IOT_ADC_CHANNEL_4, &val, IOT_ADC_EQU_MODEL_8, IOT_ADC_CUR_BAIS_DEFAULT, 256);

    //将ADC4采集值转换成电压值
    Rain_vol = IOTAdcConvertToVoltage(val);//以便数据分析与观察
    printf("雨滴传感器电压值voltage:%f \r\n", Rain_vol);
    Rain_vol = 3.4-Rain_vol;
    Rain_temp = (float)((float)Rain_vol) / 3.5 * 100;
    // Rain_temp = (float)((float)Rain_vol)*33;

    if(Rain_temp <= 0){Rain_temp = 0;}
    if(Rain_temp >= 100){Rain_temp = 100;}

    return Rain_temp;
}
