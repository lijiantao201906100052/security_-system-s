/*----------------------------------------------------------------------------
 * Copyright (c) <2018>, <Huawei Technologies Co., Ltd>
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 * conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 * of conditions and the following disclaimer in the documentation and/or other materials
 * provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 * to endorse or promote products derived from this software without specific prior written
 * permission.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
 * Notice of Export Control Law
 * ===============================================
 * Huawei LiteOS may be subject to applicable export control laws and regulations, which might
 * include those applicable to Huawei LiteOS of U.S. and the country in which you are located.
 * Import, export and usage of Huawei LiteOS in any manner by you shall be in compliance with such
 * applicable export control laws and regulations.
 *---------------------------------------------------------------------------*/
/**
 *  DATE                AUTHOR      INSTRUCTION
 *  2020-02-05 17:00  zhangqianfu  The first version
 *
 */
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <math.h>

#include "cmsis_os2.h"
#include "motor.h"
#include "iot_errno.h"
#include "iot_gpio.h"
#include "iot_pwm.h"
#include "iot_adc.h"


/* 蜂鸣器初始化
 * 烟雾传感器触发蜂鸣器1---GPIO05
 * 气体传感器触发蜂鸣器2---GPIO10
 */
void BuzzerInit(void)
{
    //烟雾传感器的蜂鸣器---GPIO05
    IoTGpioInit(IOT_GPIO_IO_GPIO_5);
    IoTIoSetFunc(IOT_GPIO_IO_GPIO_5, IOT_IO_FUNC_GPIO_5_GPIO); //设置GPIO_5的复用功能为普通GPIO
    IoTGpioSetDir(IOT_GPIO_IO_GPIO_5, IOT_GPIO_DIR_OUT);      //设置GPIO_5为输出模式

    //气体传感器的蜂鸣器---GPIO10
    IoTGpioInit(IOT_GPIO_IO_GPIO_10);
    IoTIoSetFunc(IOT_GPIO_IO_GPIO_10, IOT_IO_FUNC_GPIO_10_GPIO); //设置GPIO_10的复用功能为普通GPIO
    IoTGpioSetDir(IOT_GPIO_IO_GPIO_10, IOT_GPIO_DIR_OUT);      //设置GPIO_10为输出模式

}

/* 直流电机初始化
 * 使用直流电机模拟窗户开关
 * IN1---GPIO13---PWM4
 * IN2---GPIO07---PWM0
 */
void MotorInit(void)
{
    //IN1---GPIO13---PWM4
    IoTGpioInit(IOT_GPIO_IO_GPIO_13);
    IoTIoSetFunc(IOT_GPIO_IO_GPIO_13, IOT_IO_FUNC_GPIO_13_PWM4_OUT);
    IoTGpioSetDir(IOT_GPIO_IO_GPIO_13, IOT_GPIO_DIR_OUT);
    IoTGpioSetOutputVal(IOT_GPIO_IO_GPIO_13, IOT_GPIO_VALUE0);//低电平
    IoTPwmInit(IOT_PWM_PORT_PWM4);

    //IN2---GPIO07---PWM0
    IoTGpioInit(IOT_GPIO_IO_GPIO_7);
    IoTIoSetFunc(IOT_GPIO_IO_GPIO_7, IOT_IO_FUNC_GPIO_7_PWM0_OUT);
    IoTGpioSetDir(IOT_GPIO_IO_GPIO_7, IOT_GPIO_DIR_OUT);
    IoTGpioSetOutputVal(IOT_GPIO_IO_GPIO_7, IOT_GPIO_VALUE0);//低电平
    IoTPwmInit(IOT_PWM_PORT_PWM0);

}

/* 继电器初始化
 * 使用继电器控制风扇的开关
 * GPIO14---输出模式
 */
void FanInit(void)
{
    IoTGpioInit(IOT_GPIO_IO_GPIO_14);
    IoTIoSetFunc(IOT_GPIO_IO_GPIO_14, IOT_IO_FUNC_GPIO_14_GPIO); //设置GPIO_14的复用功能为普通GPIO
    IoTGpioSetDir(IOT_GPIO_IO_GPIO_14, IOT_GPIO_DIR_OUT);      //设置GPIO_14为输出模式
}

/* 电机初始化
 * 使用拓展板的电机模拟窗帘开关
 * GPIO08---输出模式
 */
void CurtainInit(void)
{
    // IoTGpioInit(IOT_GPIO_IO_GPIO_8);
    // IoTIoSetFunc(IOT_GPIO_IO_GPIO_8, IOT_IO_FUNC_GPIO_8_PWM1_OUT);
    // IoTGpioSetDir(IOT_GPIO_IO_GPIO_8, IOT_GPIO_DIR_OUT);
    // IoTGpioSetOutputVal(IOT_GPIO_IO_GPIO_8, IOT_GPIO_VALUE0);//低电平
    // IoTPwmInit(IOT_PWM_PORT_PWM1);
    IoTGpioInit(IOT_GPIO_IO_GPIO_8);
    IoTIoSetFunc(IOT_GPIO_IO_GPIO_8, IOT_IO_FUNC_GPIO_8_GPIO); //设置GPIO_8的复用功能为普通GPIO
    IoTGpioSetDir(IOT_GPIO_IO_GPIO_8, IOT_GPIO_DIR_OUT);      //设置GPIO_8为输出模式

}

/* 设置烟雾传感器触发蜂鸣器1的开关状态
 */
void SmokeBuzzer(Motor_Status_ENUM status)
{
    if (status == ON)
        IoTGpioSetOutputVal(IOT_GPIO_IO_GPIO_5, IOT_GPIO_VALUE0); //设置GPIO_5输出--低电平--开启蜂鸣器
    if (status == OFF)
        IoTGpioSetOutputVal(IOT_GPIO_IO_GPIO_5, IOT_GPIO_VALUE1); //设置GPIO_5输出--高电平--关闭蜂鸣器
}

/* 设置气体传感器触发蜂鸣器2的开关状态
 */
void GasBuzzer(Motor_Status_ENUM status)
{
    if (status == ON)
        IoTGpioSetOutputVal(IOT_GPIO_IO_GPIO_10, IOT_GPIO_VALUE0); //设置GPIO_10输出--低电平--开启蜂鸣器
    if (status == OFF)
        IoTGpioSetOutputVal(IOT_GPIO_IO_GPIO_10, IOT_GPIO_VALUE1); //设置GPIO_10输出--高电平--关闭蜂鸣器
}

/* 设置直流电机的开关状态
 */
void MotorStatusSet(Motor_Status_ENUM status)
{
    if (status == ON)
        IoTPwmStart(IOT_PWM_PORT_PWM4, 30, 15000);//正转--开窗
        usleep(400000);
        IoTPwmStop(IOT_PWM_PORT_PWM4);
    if (status == OFF)
        IoTPwmStart(IOT_PWM_PORT_PWM0, 30, 15000);//反转--关窗
        usleep(400000);
        IoTPwmStop(IOT_PWM_PORT_PWM0);
}

/* 设置电机的开关状态
 */
void CurtainStatusSet(Motor_Status_ENUM status)
{
    // if (status == ON)
    //     //IoTGpioSetOutputVal(IOT_GPIO_IO_GPIO_8, 1); //设置GPIO_8输出高电平打开电机
    //     IoTPwmStart(IOT_PWM_PORT_PWM1, 30, 15000);//正转--开窗
    //     usleep(400000);
    //     IoTPwmStop(IOT_PWM_PORT_PWM1);
    // if (status == OFF)
    //     //IoTGpioSetOutputVal(IOT_GPIO_IO_GPIO_8, 0); //设置GPIO_8输出低电平关闭电机
    //     IoTPwmStart(IOT_PWM_PORT_PWM1, 30, 15000);//反转--关窗
    //     usleep(400000);
    //     IoTPwmStop(IOT_PWM_PORT_PWM1);
    
    if (status == ON)
        IoTGpioSetOutputVal(IOT_GPIO_IO_GPIO_8, 1); //设置GPIO_8输出高电平打开电机
        usleep(2000000);
        IoTGpioSetOutputVal(IOT_GPIO_IO_GPIO_8, 0); //设置GPIO_8输出低电平关闭电机
    if (status == OFF)
        IoTGpioSetOutputVal(IOT_GPIO_IO_GPIO_8, 1); //设置GPIO_8输出高电平打开电机
        usleep(2000000);
        IoTGpioSetOutputVal(IOT_GPIO_IO_GPIO_8, 0); //设置GPIO_8输出低电平关闭电机
}

/* 设置继电器的开关状态
 */
void FanStatusSet(Motor_Status_ENUM status)
{
    if (status == ON)
        IoTGpioSetOutputVal(IOT_GPIO_IO_GPIO_14, IOT_GPIO_VALUE0); //设置GPIO_14输出--低电平--开启风扇
    if (status == OFF)
        IoTGpioSetOutputVal(IOT_GPIO_IO_GPIO_14, IOT_GPIO_VALUE1); //设置GPIO_14输出--高电平--关闭风扇
}

